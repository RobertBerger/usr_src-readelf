# ELDK/Yocto sets up some funny Python stuff, 
# which keeps my test cases from running
# This is a work around which seems to work
(
source ../../../env.sh

if [ "${TRAINING_ID_SHORT}" = "yocto" ]; then
   export TESTARGS="-i $BOARDIP -p $HOME"
else #intely?
   export TESTARGS="-i $BOARDIP"
fi

echo "echo \"+ ./test_1.py $TESTARGS"\" > test.sh
echo "./test_1.py $TESTARGS" >> test.sh

echo "echo \"+ ./test_2.py $TESTARGS"\" >> test.sh
echo "./test_2.py $TESTARGS" >> test.sh

echo "echo \"+ ./test_3.py $TESTARGS"\" >> test.sh
echo "./test_3.py $TESTARGS" >> test.sh

echo "echo \"+ ./test_4.py $TESTARGS"\" >> test.sh
echo "./test_4.py $TESTARGS" >> test.sh

chmod +x test.sh
)
./test.sh
rm -f test.sh
