#!/usr/bin/python3

import sys
from pexpect import pxssh
import getpass
import os
import argparse

user='root'
try:
  parser = argparse.ArgumentParser()

  parser.add_argument("-ip", "--ip_addr", type=str, required=True)
  parser.add_argument("-p" , "--pwd"    , type=str, required=False)
  #parser.add_argument("-d", "--integer", type=int, default=50)

  args = parser.parse_args()
  # if we explicitly pass the (target)_pwd:
  if args.pwd:
    print('IP_ADDR: ' + args.ip_addr)
    print('PWD: ' + args.pwd)
  else:
    print('IP_ADDR: ' + args.ip_addr)

  host = args.ip_addr
  target_dir = args.pwd
  host_pwd = os.getcwd()
  #print 'host_pwd: ' + host_pwd
  host_pwd_back = host_pwd + '/../../'
  #print 'host_pwd_back: ' + host_pwd_back
  rel_pwd= os.path.relpath(host_pwd, host_pwd_back)
  #print 'rel_pwd: ' + rel_pwd
except IndexError:
  print('Must have host name or ip address')
  sys.exit(1)
password = 'yourpassword'
try:
  s = pxssh.pxssh()
  s.force_password = True
  s.login (host, user, password, login_timeout=20)

  # if we pass (target)_pwd we adjust for that
  if args.pwd:
    pwd = target_dir + '/' + rel_pwd
  else:
    pwd = os.popen('pwd').read().strip('\n')

  # this is the (target)_pwd
  # print('target pwd: ' + pwd)
  working_dir = pwd + '/../'
  cmd='cd '+ working_dir
  s.sendline (cmd)
  s.prompt()

  cmd='./03.03_run.sh DONT_WAIT'
  s.sendline (cmd)
  s.prompt()
  #print(s.before,s.after)
  print(s.before.decode('utf-8', 'ignore'), s.after.decode('utf-8', 'ignore'))

#  cmd='dmesg | tail -34'
#  s.sendline (cmd)
#  s.prompt()
#  print s.before,s.after

#  s.interact()
except pxssh.ExceptionPxssh as e:
  print("pxssh failed on login.")
  print(str(e))
except OSError:
  print("End session to " + user + "@" + host)

