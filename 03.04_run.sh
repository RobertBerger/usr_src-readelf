#!/bin/bash 

_DEBUG="on"
function DEBUG()
{
 [ "$_DEBUG" == "on" ] &&  $@
}

source local-config.mak

ANALYZE="$PROJECT_NAME.out"
ANALYZE1="$PROJECT_NAME.out.stripped"
SYMBOL=".rodata"
SYMBOL1="PROGBITS"
EXT="readelf"

echo "--> ${ANALYZE}, ${ANALYZE1}"

echo "=========="
echo "inspect ${ANALYZE}.${EXT} file:"
echo "cat ${ANALYZE}.${EXT} | grep ${SYMBOL} | grep ${SYMBOL1}"
cat ${ANALYZE}.${EXT} | grep ${SYMBOL} | grep ${SYMBOL1}
if [[ $1 != "DONT_WAIT" ]]; then
   echo "enter the section number e.g. 15"
   read r
else
   r="15"
fi
echo "readelf -x $r ${ANALYZE}"
readelf -x $r ${ANALYZE}
echo "=========="
echo "=========="
echo "inspect ${ANALYZE1}.${EXT} file:"
echo "cat ${ANALYZE1}.${EXT} | grep ${SYMBOL} | grep ${SYMBOL1}"
cat ${ANALYZE1}.${EXT} | grep ${SYMBOL} | grep ${SYMBOL1}
if [[ $1 != "DONT_WAIT" ]]; then
   echo "enter the section number e.g. 15"
   read r
else
   r="15"
fi
echo "readelf -x $r ${ANALYZE1}"
readelf -x $r ${ANALYZE1}
echo "=========="

echo "<-- ${ANALYZE}, ${ANALYZE1}"
