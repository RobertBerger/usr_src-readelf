#!/bin/bash 

_DEBUG="on"
function DEBUG()
{
 [ "$_DEBUG" == "on" ] &&  $@
}

source local-config.mak

ANALYZE="$PROJECT_NAME.out"
ANALYZE1="$PROJECT_NAME.out.stripped"
SYMBOL="hello_world"
EXT="readelf"

echo "--> ${ANALYZE}, ${ANALYZE1}"

echo "=========="
echo "inspect ${ANALYZE}.${EXT} file:"
echo "cat ${ANALYZE}.${EXT} | grep ${SYMBOL}"
cat ${ANALYZE}.${EXT} | grep ${SYMBOL}
echo "=========="
echo "=========="
echo "inspect ${ANALYZE1}.${EXT} file:"
echo "cat ${ANALYZE1}.${EXT} | grep ${SYMBOL}"
cat ${ANALYZE1}.${EXT} | grep ${SYMBOL}
echo "=========="

echo "<-- ${ANALYZE}, ${ANALYZE1}"
