#!/bin/bash
# TAG_TMP is a temporary file
# INCLUDE_FILE_CREATES is the include file,
# which needs to be included by ic_common_ver.c
# and which will be filled in with
# #define TAG something

#TAG_TMP=tag.txt
INCLUDE_FILE_CREATED=git_info.h

#git show | grep tag > string

#perl test.pl < string | tr "'", " " > $TAG_TMP

#LOCAL_TAG=`cat $TAG_TMP`

GIT_DESCRIBE_ALL=`git describe --all`
SETLOCALVERSION=`./setlocalversion.sh`

LOCAL_TAG="$GIT_DESCRIBE_ALL$SETLOCALVERSION"

echo "/* This is automatically generated - do not modify */" > $INCLUDE_FILE_CREATED
echo "#define TAG \"$LOCAL_TAG\"" >> $INCLUDE_FILE_CREATED

echo "found the TAG: \"$LOCAL_TAG\""

