#!/bin/bash 

_DEBUG="on"
function DEBUG()
{
 [ "$_DEBUG" == "on" ] &&  $@
}

#export DEVKIT=native

source ../../env.sh
source local-config.mak

./10_clean.sh

DEBUG set -x

if [[ $1 != "DONT_WAIT" ]]; then
  vim $PROJECT_NAME.c
fi
echo "build $PROJECT_NAME"
./find_tag.sh
make
make strip

DEBUG set +x
