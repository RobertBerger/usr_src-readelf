#!/bin/bash 

_DEBUG="on"
function DEBUG()
{
 [ "$_DEBUG" == "on" ] &&  $@
}

source local-config.mak

ANALYZE="$PROJECT_NAME.out.stripped"
EXT="readelf"

echo "--> ${ANALYZE}"

echo "=========="
echo "./${ANALYZE}"
./${ANALYZE}
echo "=========="
echo "create .${EXT} file:"
echo "readelf -a  ${ANALYZE} > ${ANALYZE}.${EXT}"
readelf -a  ${ANALYZE} > ${ANALYZE}.${EXT}
echo "=========="
echo "inspect .${EXT} file:"
echo "cat ${ANALYZE}.${EXT}"
cat ${ANALYZE}.${EXT}
echo "=========="

echo "<-- ${ANALYZE}"
