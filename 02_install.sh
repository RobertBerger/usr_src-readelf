OLD_PATH=$PATH
PWD=$(pwd)

source ../../env.sh

sudo mkdir -p $NFS_EXPORTED_ROOFS_ROOT/$(pwd)
sudo chown -R $USER:$USER $NFS_EXPORTED_ROOFS_ROOT/$(pwd)
cp -r * $NFS_EXPORTED_ROOFS_ROOT/$(pwd)

PATH=$OLD_PATH
cd $PWD
